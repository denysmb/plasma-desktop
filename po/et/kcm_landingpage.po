# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the plasma-desktop package.
#
# Mihkel Tõnnov <mihhkel@gmail.com>, 2022.
msgid ""
msgstr ""
"Project-Id-Version: plasma-desktop\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-04-09 02:10+0000\n"
"PO-Revision-Date: 2022-11-15 20:39+0100\n"
"Last-Translator: Mihkel Tõnnov <mihhkel@gmail.com>\n"
"Language-Team: Estonian <>\n"
"Language: et\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 22.08.3\n"

#. i18n: ectx: label, entry (colorScheme), group (General)
#: landingpage_kdeglobalssettings.kcfg:9
#, kde-format
msgid "Color scheme name"
msgstr "Värviskeemi nimi"

#. i18n: ectx: label, entry (singleClick), group (KDE)
#: landingpage_kdeglobalssettings.kcfg:15
#, kde-format
msgid "Single click to open files"
msgstr "Ühekordne klõps failide avamiseks"

#. i18n: ectx: label, entry (lookAndFeelPackage), group (KDE)
#: landingpage_kdeglobalssettings.kcfg:19
#, kde-format
msgid "Global Look and Feel package"
msgstr "Üleüldise välimuse pakett"

#. i18n: ectx: label, entry (defaultLightLookAndFeel), group (KDE)
#. i18n: ectx: label, entry (defaultDarkLookAndFeel), group (KDE)
#: landingpage_kdeglobalssettings.kcfg:23
#: landingpage_kdeglobalssettings.kcfg:27
#, kde-format
msgid "Global Look and Feel package, alternate"
msgstr "Alternatiivne üleüldise välimuse pakett"

#. i18n: ectx: label, entry (animationDurationFactor), group (KDE)
#: landingpage_kdeglobalssettings.kcfg:31
#, kde-format
msgid "Animation speed"
msgstr "Animatsiooni kiirus"

#: package/contents/ui/main.qml:27
#, kde-format
msgid "Theme:"
msgstr "Kujundus:"

#: package/contents/ui/main.qml:84
#, kde-format
msgid "Animation speed:"
msgstr "Animatsiooni kiirus:"

#: package/contents/ui/main.qml:93
#, kde-format
msgctxt "Animation speed"
msgid "Slow"
msgstr "Aeglane"

#: package/contents/ui/main.qml:99
#, kde-format
msgctxt "Animation speed"
msgid "Instant"
msgstr "Silmapilkne"

#: package/contents/ui/main.qml:113
#, kde-format
msgid "Change Wallpaper…"
msgstr "Vaheta taustapilti …"

#: package/contents/ui/main.qml:120
#, kde-format
msgid "More Appearance Settings…"
msgstr "Välimuse lisaseadistused …"

#: package/contents/ui/main.qml:135
#, kde-format
msgctxt ""
"part of a sentence: 'Clicking files or folders [opens them/selects them]'"
msgid "Clicking files or folders:"
msgstr "Ühekordne klõps failil või kataloogil:"

#: package/contents/ui/main.qml:136
#, kde-format
msgctxt "part of a sentence: 'Clicking files or folders opens them'"
msgid "Opens them"
msgstr "Avab selle"

#: package/contents/ui/main.qml:141
#, kde-format
msgid "Select by clicking on item's selection marker"
msgstr "Valimine klõpsuga elemendi valikumärgisele"

#: package/contents/ui/main.qml:162
#, kde-format
msgctxt "part of a sentence: 'Clicking files or folders selects them'"
msgid "Selects them"
msgstr "Valib selle"

#: package/contents/ui/main.qml:167
#, kde-format
msgid "Open by double-clicking instead"
msgstr "Avamiseks tuleb teha topeltklõps"

#: package/contents/ui/main.qml:190
#, kde-format
msgid "More Behavior Settings…"
msgstr "Käitumise lisaseadistused …"

#: package/contents/ui/main.qml:202
#, kde-format
msgid "Most Used Pages:"
msgstr "Enim tarvitatud leheküljed:"

#~ msgid "Plasma"
#~ msgstr "Plasma"

#~ msgid ""
#~ "You can help KDE improve Plasma by contributing information on how you "
#~ "use it, so we can focus on things that matter to you.<br/><br/"
#~ ">Contributing this information is optional and entirely anonymous. We "
#~ "never collect your personal data, files you use, websites you visit, or "
#~ "information that could identify you."
#~ msgstr ""
#~ "Sa võid abistada KDE arendajaid Plasma täiustamisel, kui jagad nendega "
#~ "teavet, mil moel sina seda kasutad, mis võimaldab meil keskenduda just "
#~ "sulle vajalikele asjadele.<br/><br/>Selle teabe edastamine ei ole kuidagi "
#~ "kohustuslik ja on täiesti anonüümne. Me ei kogu mingil juhul sinu "
#~ "isiklikke andmeid, teavet failide kohta, mida kasutad, ega veebilehtede "
#~ "kohta, mida külastad, samuti mitte mingit muud teavet, mis võimaldaks "
#~ "sind tuvastada."

#~ msgid "No data will be sent."
#~ msgstr "Mingeid andmeid ei edastata."

#~ msgid "The following information will be sent:"
#~ msgstr "Edastatakse järgmine teave:"

#~ msgid "Send User Feedback:"
#~ msgstr "Kasutaja tagasiside saatmine:"
