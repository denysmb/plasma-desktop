# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
# Adrián Chaves Fernández <adriyetichaves@gmail.com>, 2015, 2016.
# Adrián Chaves (Gallaecio) <adrian@chaves.io>, 2017, 2018, 2019, 2023.
#
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-03-23 02:36+0000\n"
"PO-Revision-Date: 2023-04-06 21:41+0200\n"
"Last-Translator: Adrián Chaves (Gallaecio) <adrian@chaves.io>\n"
"Language-Team: Galician <proxecto@trasno.gal>\n"
"Language: gl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 22.12.3\n"

#: package/contents/config/config.qml:16
#, kde-format
msgid "Location"
msgstr "Lugar"

#: package/contents/config/config.qml:23
#: package/contents/ui/FolderViewLayer.qml:429
#, kde-format
msgid "Icons"
msgstr "Iconas"

#: package/contents/config/config.qml:30
#, kde-format
msgid "Filter"
msgstr "Filtro"

#: package/contents/ui/BackButtonItem.qml:103
#, kde-format
msgid "Back"
msgstr "Volver"

#: package/contents/ui/ConfigFilter.qml:64
#, kde-format
msgid "Files:"
msgstr "Ficheiros:"

#: package/contents/ui/ConfigFilter.qml:65
#, kde-format
msgid "Show all"
msgstr "Mostralo todo"

#: package/contents/ui/ConfigFilter.qml:65
#, kde-format
msgid "Show matching"
msgstr "Mostrar as coincidencias"

#: package/contents/ui/ConfigFilter.qml:65
#, kde-format
msgid "Hide matching"
msgstr "Agochar as coincidencias"

#: package/contents/ui/ConfigFilter.qml:70
#, kde-format
msgid "File name pattern:"
msgstr "Padrón do nome do ficheiro:"

#: package/contents/ui/ConfigFilter.qml:77
#, kde-format
msgid "File types:"
msgstr "Tipos de ficheiro:"

#: package/contents/ui/ConfigFilter.qml:83
#, kde-format
msgid "Show hidden files:"
msgstr "Mostrar os ficheiros agochados:"

#: package/contents/ui/ConfigFilter.qml:179
#, kde-format
msgid "File type"
msgstr "Tipo de ficheiro"

#: package/contents/ui/ConfigFilter.qml:188
#, kde-format
msgid "Description"
msgstr "Descrición"

#: package/contents/ui/ConfigFilter.qml:201
#, kde-format
msgid "Select All"
msgstr "Seleccionalo todo"

#: package/contents/ui/ConfigFilter.qml:211
#, kde-format
msgid "Deselect All"
msgstr "Anular a selección"

#: package/contents/ui/ConfigIcons.qml:62
#, kde-format
msgid "Panel button:"
msgstr "Botón do panel:"

#: package/contents/ui/ConfigIcons.qml:68
#, kde-format
msgid "Use a custom icon"
msgstr "Usar unha icona personalizada."

#: package/contents/ui/ConfigIcons.qml:101
#, kde-format
msgctxt "@item:inmenu Open icon chooser dialog"
msgid "Choose…"
msgstr "Escoller…"

#: package/contents/ui/ConfigIcons.qml:107
#, kde-format
msgctxt "@item:inmenu Reset icon to default"
msgid "Clear Icon"
msgstr "Borrar a icona"

#: package/contents/ui/ConfigIcons.qml:127
#, kde-format
msgid "Arrangement:"
msgstr "Disposición:"

#: package/contents/ui/ConfigIcons.qml:131
#, kde-format
msgctxt "@item:inlistbox arrangement of icons"
msgid "Left to Right"
msgstr "Da esquerda á dereita"

#: package/contents/ui/ConfigIcons.qml:132
#, kde-format
msgctxt "@item:inlistbox arrangement of icons"
msgid "Right to Left"
msgstr "Da dereita á esquerda"

#: package/contents/ui/ConfigIcons.qml:133
#, kde-format
msgctxt "@item:inlistbox arrangement of icons"
msgid "Top to Bottom"
msgstr "De arriba a abaixo"

#: package/contents/ui/ConfigIcons.qml:142
#, kde-format
msgctxt "@item:inlistbox alignment of icons"
msgid "Align left"
msgstr "Aliñar á esquerda"

#: package/contents/ui/ConfigIcons.qml:143
#, kde-format
msgctxt "@item:inlistbox alignment of icons"
msgid "Align right"
msgstr "Aliñar á dereita"

#: package/contents/ui/ConfigIcons.qml:158
#, kde-format
msgid "Lock in place"
msgstr "Bloquear no sitio"

#: package/contents/ui/ConfigIcons.qml:172
#, kde-format
msgid "Sorting:"
msgstr "Orde:"

#: package/contents/ui/ConfigIcons.qml:180
#, kde-format
msgctxt "@item:inlistbox sort icons manually"
msgid "Manual"
msgstr "Manual"

#: package/contents/ui/ConfigIcons.qml:181
#, kde-format
msgctxt "@item:inlistbox sort icons by name"
msgid "Name"
msgstr "Nome"

#: package/contents/ui/ConfigIcons.qml:182
#, kde-format
msgctxt "@item:inlistbox sort icons by size"
msgid "Size"
msgstr "Tamaño"

#: package/contents/ui/ConfigIcons.qml:183
#, kde-format
msgctxt "@item:inlistbox sort icons by file type"
msgid "Type"
msgstr "Tipo"

#: package/contents/ui/ConfigIcons.qml:184
#, kde-format
msgctxt "@item:inlistbox sort icons by date"
msgid "Date"
msgstr "Data"

#: package/contents/ui/ConfigIcons.qml:195
#, kde-format
msgctxt "@option:check sort icons in descending order"
msgid "Descending"
msgstr "Descendente"

#: package/contents/ui/ConfigIcons.qml:203
#, kde-format
msgctxt "@option:check sort icons with folders first"
msgid "Folders first"
msgstr "Primeiro os cartafoles"

#: package/contents/ui/ConfigIcons.qml:217
#, kde-format
msgctxt "whether to use icon or list view"
msgid "View mode:"
msgstr "Modo da vista:"

#: package/contents/ui/ConfigIcons.qml:219
#, kde-format
msgctxt "@item:inlistbox show icons in a list"
msgid "List"
msgstr "Lista"

#: package/contents/ui/ConfigIcons.qml:220
#, kde-format
msgctxt "@item:inlistbox show icons in a grid"
msgid "Grid"
msgstr "Grade"

#: package/contents/ui/ConfigIcons.qml:231
#, kde-format
msgid "Icon size:"
msgstr "Tamaño das iconas:"

#: package/contents/ui/ConfigIcons.qml:246
#, kde-format
msgctxt "@label:slider smallest icon size"
msgid "Small"
msgstr "Pequeno"

#: package/contents/ui/ConfigIcons.qml:255
#, kde-format
msgctxt "@label:slider largest icon size"
msgid "Large"
msgstr "Grande"

#: package/contents/ui/ConfigIcons.qml:264
#, kde-format
msgid "Label width:"
msgstr "Anchura das etiquetas:"

#: package/contents/ui/ConfigIcons.qml:267
#, kde-format
msgctxt "@item:inlistbox how long a text label should be"
msgid "Narrow"
msgstr "Estreita"

#: package/contents/ui/ConfigIcons.qml:268
#, kde-format
msgctxt "@item:inlistbox how long a text label should be"
msgid "Medium"
msgstr "Media"

#: package/contents/ui/ConfigIcons.qml:269
#, kde-format
msgctxt "@item:inlistbox how long a text label should be"
msgid "Wide"
msgstr "Ancha"

#: package/contents/ui/ConfigIcons.qml:277
#, kde-format
msgid "Text lines:"
msgstr "Liñas de texto:"

#: package/contents/ui/ConfigIcons.qml:293
#, kde-format
msgid "When hovering over icons:"
msgstr "Ao cubrir iconas:"

#: package/contents/ui/ConfigIcons.qml:295
#, kde-format
msgid "Show tooltips"
msgstr "Mostrar as axudas emerxentes"

#: package/contents/ui/ConfigIcons.qml:302
#, kde-format
msgid "Show selection markers"
msgstr "Mostrar os marcadores de selección"

#: package/contents/ui/ConfigIcons.qml:309
#, kde-format
msgid "Show folder preview popups"
msgstr "Mostrar as vistas previas emerxentes dos cartafoles"

#: package/contents/ui/ConfigIcons.qml:319
#, kde-format
msgid "Rename:"
msgstr "Cambiar de nome:"

#: package/contents/ui/ConfigIcons.qml:323
#, kde-format
msgid "Rename inline by clicking selected item's text"
msgstr "Cambiar de nome in situ premendo o texto do elemento seleccionado"

#: package/contents/ui/ConfigIcons.qml:334
#, kde-format
msgid "Previews:"
msgstr "Vistas previas:"

#: package/contents/ui/ConfigIcons.qml:336
#, kde-format
msgid "Show preview thumbnails"
msgstr "Mostras as miniaturas de vista previa"

#: package/contents/ui/ConfigIcons.qml:344
#, kde-format
msgid "Configure Preview Plugins…"
msgstr "Configurar os complementos de vista previa…"

#: package/contents/ui/ConfigLocation.qml:81
#, kde-format
msgid "Show:"
msgstr "Mostrar:"

#: package/contents/ui/ConfigLocation.qml:83
#, kde-format
msgid "Desktop folder"
msgstr "O cartafol do escritorio"

#: package/contents/ui/ConfigLocation.qml:90
#, kde-format
msgid "Files linked to the current activity"
msgstr "Os ficheiros asociados coa actividade actual"

#: package/contents/ui/ConfigLocation.qml:97
#, kde-format
msgid "Places panel item:"
msgstr "Elemento do panel de lugares:"

#: package/contents/ui/ConfigLocation.qml:130
#, kde-format
msgid "Custom location:"
msgstr "Lugar personalizado:"

#: package/contents/ui/ConfigLocation.qml:138
#, kde-format
msgid "Type path or URL…"
msgstr "Escriba unha ruta ou URL…"

#: package/contents/ui/ConfigLocation.qml:181
#, kde-format
msgid "Title:"
msgstr "Título:"

#: package/contents/ui/ConfigLocation.qml:183
#, kde-format
msgid "None"
msgstr "Ningunha"

#: package/contents/ui/ConfigLocation.qml:183
#, kde-format
msgid "Default"
msgstr "Predeterminada"

#: package/contents/ui/ConfigLocation.qml:183
#, kde-format
msgid "Full path"
msgstr "Ruta completa"

#: package/contents/ui/ConfigLocation.qml:183
#, kde-format
msgid "Custom title"
msgstr "Título personalizado"

#: package/contents/ui/ConfigLocation.qml:198
#, kde-format
msgid "Enter custom title…"
msgstr "Escriba un título personalizado…"

#: package/contents/ui/ConfigOverlay.qml:91
#, kde-format
msgid "Rotate"
msgstr "Rotar"

#: package/contents/ui/ConfigOverlay.qml:181
#, kde-format
msgid "Open Externally"
msgstr "Abrir externamente"

#: package/contents/ui/ConfigOverlay.qml:193
#, kde-format
msgid "Hide Background"
msgstr "Agochar o fondo"

#: package/contents/ui/ConfigOverlay.qml:193
#, kde-format
msgid "Show Background"
msgstr "Mostrar o fondo"

#: package/contents/ui/ConfigOverlay.qml:241
#, kde-format
msgid "Remove"
msgstr "Retirar"

#: package/contents/ui/FolderItemPreviewPluginsDialog.qml:19
#, kde-format
msgid "Preview Plugins"
msgstr "Vistas previas dos complementos"

#: package/contents/ui/main.qml:355
#, kde-format
msgid "Configure Desktop and Wallpaper…"
msgstr "Configurar o escritorio e o fondo…"

#: plugins/folder/directorypicker.cpp:32
#, kde-format
msgid "Select Folder"
msgstr "Seleccionar o cartafol"

#: plugins/folder/foldermodel.cpp:468
#, kde-format
msgid "&Refresh Desktop"
msgstr "&Actualizar o escritorio"

#: plugins/folder/foldermodel.cpp:468 plugins/folder/foldermodel.cpp:1624
#, kde-format
msgid "&Refresh View"
msgstr "&Actualizar a vista"

#: plugins/folder/foldermodel.cpp:1633
#, kde-format
msgid "&Empty Trash"
msgstr "Bal&eirar o lixo"

#: plugins/folder/foldermodel.cpp:1636
#, kde-format
msgctxt "Restore from trash"
msgid "Restore"
msgstr "Restaurar"

#: plugins/folder/foldermodel.cpp:1639
#, kde-format
msgid "&Open"
msgstr "&Abrir"

#: plugins/folder/foldermodel.cpp:1756
#, kde-format
msgid "&Paste"
msgstr "&Pegar"

#: plugins/folder/foldermodel.cpp:1873
#, kde-format
msgid "&Properties"
msgstr "&Propiedades"

#: plugins/folder/viewpropertiesmenu.cpp:21
#, kde-format
msgid "Sort By"
msgstr "Ordenar por"

#: plugins/folder/viewpropertiesmenu.cpp:24
#, kde-format
msgctxt "@item:inmenu Sort icons manually"
msgid "Unsorted"
msgstr "Sen ordenar"

#: plugins/folder/viewpropertiesmenu.cpp:28
#, kde-format
msgctxt "@item:inmenu Sort icons by name"
msgid "Name"
msgstr "Nome"

#: plugins/folder/viewpropertiesmenu.cpp:32
#, kde-format
msgctxt "@item:inmenu Sort icons by size"
msgid "Size"
msgstr "Tamaño"

#: plugins/folder/viewpropertiesmenu.cpp:36
#, kde-format
msgctxt "@item:inmenu Sort icons by file type"
msgid "Type"
msgstr "Tipo"

#: plugins/folder/viewpropertiesmenu.cpp:40
#, kde-format
msgctxt "@item:inmenu Sort icons by date"
msgid "Date"
msgstr "Data"

#: plugins/folder/viewpropertiesmenu.cpp:45
#, kde-format
msgctxt "@item:inmenu Sort icons in descending order"
msgid "Descending"
msgstr "Descendente"

#: plugins/folder/viewpropertiesmenu.cpp:47
#, kde-format
msgctxt "@item:inmenu Sort icons with folders first"
msgid "Folders First"
msgstr "Primeiro os cartafoles"

#: plugins/folder/viewpropertiesmenu.cpp:50
#, kde-format
msgid "Icon Size"
msgstr "Tamaño das iconas"

#: plugins/folder/viewpropertiesmenu.cpp:53
#, kde-format
msgctxt "@item:inmenu size of the icons"
msgid "Tiny"
msgstr "Pequerrecho"

#: plugins/folder/viewpropertiesmenu.cpp:54
#, kde-format
msgctxt "@item:inmenu size of the icons"
msgid "Very Small"
msgstr "Moi pequeno"

#: plugins/folder/viewpropertiesmenu.cpp:55
#, kde-format
msgctxt "@item:inmenu size of the icons"
msgid "Small"
msgstr "Pequeno"

#: plugins/folder/viewpropertiesmenu.cpp:56
#, kde-format
msgctxt "@item:inmenu size of the icons"
msgid "Small-Medium"
msgstr "Medianiño"

#: plugins/folder/viewpropertiesmenu.cpp:57
#, kde-format
msgctxt "@item:inmenu size of the icons"
msgid "Medium"
msgstr "Mediano"

#: plugins/folder/viewpropertiesmenu.cpp:58
#, kde-format
msgctxt "@item:inmenu size of the icons"
msgid "Large"
msgstr "Grande"

#: plugins/folder/viewpropertiesmenu.cpp:59
#, kde-format
msgctxt "@item:inmenu size of the icons"
msgid "Huge"
msgstr "Enorme"

#: plugins/folder/viewpropertiesmenu.cpp:67
#, kde-format
msgctxt "@item:inmenu arrangement of icons"
msgid "Arrange"
msgstr "Dispor"

#: plugins/folder/viewpropertiesmenu.cpp:71
#, kde-format
msgctxt "@item:inmenu arrangement of icons"
msgid "Left to Right"
msgstr "Da esquerda á dereita"

#: plugins/folder/viewpropertiesmenu.cpp:72
#, kde-format
msgctxt "@item:inmenu arrangement of icons"
msgid "Right to Left"
msgstr "Da dereita á esquerda"

#: plugins/folder/viewpropertiesmenu.cpp:76
#, kde-format
msgctxt "@item:inmenu arrangement of icons"
msgid "Top to Bottom"
msgstr "De arriba a abaixo"

#: plugins/folder/viewpropertiesmenu.cpp:81
#, kde-format
msgid "Align"
msgstr "Aliñar"

#: plugins/folder/viewpropertiesmenu.cpp:84
#, kde-format
msgctxt "@item:inmenu alignment of icons"
msgid "Left"
msgstr "Esquerda"

#: plugins/folder/viewpropertiesmenu.cpp:88
#, kde-format
msgctxt "@item:inmenu alignment of icons"
msgid "Right"
msgstr "Dereita"

#: plugins/folder/viewpropertiesmenu.cpp:93
#, kde-format
msgid "Show Previews"
msgstr "Mostrar vistas previas"

#: plugins/folder/viewpropertiesmenu.cpp:97
#, kde-format
msgctxt "@item:inmenu lock icon positions in place"
msgid "Locked"
msgstr "Bloqueado"

#~ msgid "OK"
#~ msgstr "Aceptar"

#~ msgid "Cancel"
#~ msgstr "Cancelar"

#, fuzzy
#~| msgid "Rows"
#~ msgctxt "@item:inlistbox arrangement of icons"
#~ msgid "Rows"
#~ msgstr "Filas"

#, fuzzy
#~| msgid "Columns"
#~ msgctxt "@item:inlistbox arrangement of icons"
#~ msgid "Columns"
#~ msgstr "Columnas"

#, fuzzy
#~| msgid "Rows"
#~ msgctxt "@item:inmenu arrangement of icons"
#~ msgid "Rows"
#~ msgstr "Filas"

#, fuzzy
#~| msgid "Columns"
#~ msgctxt "@item:inmenu arrangement of icons"
#~ msgid "Columns"
#~ msgstr "Columnas"

#~ msgid "Features:"
#~ msgstr "Funcionalidades:"

#, fuzzy
#~| msgid "Search..."
#~ msgid "Search…"
#~ msgstr "Buscar…"

#, fuzzy
#~| msgid "Create &Folder..."
#~ msgid "&Create Folder"
#~ msgstr "Crear un &cartafol…"

#~ msgid "Tweaks"
#~ msgstr "Axustes"

#~ msgid ""
#~ "With the Desktop Toolbox hidden, right-click on the desktop and choose "
#~ "'Configure Desktop...' to access this configuration window in the future"
#~ msgstr ""
#~ "Coa caixa de ferramentas do escritorio agochada, faga clic dereito no "
#~ "escritorio e seleccione «Configurar o escritorio…» para acceder a esta "
#~ "xanela de configuración no futuro"

#~ msgid "Show the desktop toolbox"
#~ msgstr "Mostrar a caixa de ferramentas do escritorio"

#~ msgid "Press and hold widgets to move them and reveal their handles"
#~ msgstr ""
#~ "Premer un trebello e manter premido para movelo e mostrar as súas asas."

#~ msgid "Widgets unlocked"
#~ msgstr "Desbloqueáronse os trebellos."

#~ msgid ""
#~ "You can press and hold widgets to move them and reveal their handles."
#~ msgstr ""
#~ "Prema un trebello e manteña premido para movelo e mostrar as súas asas."

#~ msgid "Got it"
#~ msgstr "Xa o collín"

#~ msgid "Resize"
#~ msgstr "Cambiar o tamaño"

#~ msgid "Size:"
#~ msgstr "Tamaño:"

#~ msgid "Desktop Layout"
#~ msgstr "Disposición do escritorio"

#~ msgid "Widget Handling"
#~ msgstr "Xestión dos trebellos"

#~ msgid "Arrange in"
#~ msgstr "Dispor en"

#~ msgid "Sort by"
#~ msgstr "Ordenar por"

#~ msgid "Appearance:"
#~ msgstr "Aparencia:"

#~ msgid "Location:"
#~ msgstr "Lugar:"

#~ msgid "Show the Desktop folder"
#~ msgstr "Mostrar o cartafol do escritorio"

#~ msgid "Specify a folder:"
#~ msgstr "Especifique un cartafol:"

#~ msgid "&Reload"
#~ msgstr "&Recargar"

#~ msgid "&Move to Trash"
#~ msgstr "&Botar no lixo"

#~ msgid "&Delete"
#~ msgstr "&Eliminar"

#~ msgid "Align:"
#~ msgstr "Aliñar:"

#~ msgid "Sorting"
#~ msgstr "Orde"

#~ msgid "Up"
#~ msgstr "Subir"

#~ msgid ""
#~ "Tweaks are experimental options that may become defaults depending on "
#~ "your feedback."
#~ msgstr ""
#~ "Os axustes son opcións experimentais que poderían converterse en "
#~ "predeterminadas se lle parece ben."

#~ msgid "Show Original Directory"
#~ msgstr "Mostrar o cartafol orixinal"

#~ msgid "Show Original File"
#~ msgstr "Mostrar o ficheiro orixinal"

#~ msgid "&Configure Trash Bin"
#~ msgstr "&Configurar o lixo"

#~ msgid "&Bookmark This Page"
#~ msgstr "Engadir a páxina aos &marcadores"

#~ msgid "&Bookmark This Location"
#~ msgstr "Engadir o lugar aos &marcadores"

#~ msgid "&Bookmark This Folder"
#~ msgstr "Engadir o cartafol aos &marcadores"

#~ msgid "&Bookmark This Link"
#~ msgstr "Engadir a ligazón aos &marcadores"

#~ msgid "&Bookmark This File"
#~ msgstr "Engadir o ficheiro aos &marcadores"

#~ msgctxt "@title:menu"
#~ msgid "Copy To"
#~ msgstr "Copiar a"

#~ msgctxt "@title:menu"
#~ msgid "Move To"
#~ msgstr "Mover a"

#~ msgctxt "@title:menu"
#~ msgid "Home Folder"
#~ msgstr "Cartafol persoal"

#~ msgctxt "@title:menu"
#~ msgid "Root Folder"
#~ msgstr "Cartafol raíz"

#~ msgctxt "@title:menu in Copy To or Move To submenu"
#~ msgid "Browse..."
#~ msgstr "Examinar…"

#~ msgctxt "@title:menu"
#~ msgid "Copy Here"
#~ msgstr "Copiar aquí"

#~ msgctxt "@title:menu"
#~ msgid "Move Here"
#~ msgstr "Mover aquí"

#~ msgid "Share"
#~ msgstr "Compartir"

#~ msgid "Icon:"
#~ msgstr "Icona:"
