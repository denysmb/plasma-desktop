# translation of kcmkeyboard.po to Dutch
# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Rinse de Vries <rinsedevries@kde.nl>, 2007, 2008.
# Freek de Kruijf <freekdekruijf@kde.nl>, 2010, 2011, 2012, 2014, 2016, 2017, 2020, 2021.
msgid ""
msgstr ""
"Project-Id-Version: kcmkeyboard\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-03-17 02:33+0000\n"
"PO-Revision-Date: 2021-10-14 11:30+0200\n"
"Last-Translator: Freek de Kruijf <freekdekruijf@kde.nl>\n"
"Language-Team: \n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 21.08.1\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Freek de Kruijf - t/m 2021"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "freekdekruijf@kde.nl"

#: bindings.cpp:24
#, kde-format
msgid "Keyboard Layout Switcher"
msgstr "Omschakelaar van toetsenbordindeling"

#: bindings.cpp:26
#, kde-format
msgid "Switch to Next Keyboard Layout"
msgstr "Naar volgende toetsenbordindeling schakelen"

#: bindings.cpp:49
#, kde-format
msgid "Switch keyboard layout to %1"
msgstr "Toetsenbordindeling omschakelen naar %1"

#: flags.cpp:122
#, kde-format
msgctxt "layout - variant"
msgid "%1 - %2"
msgstr "%1 - %2"

#. i18n: ectx: property (windowTitle), widget (QDialog, AddLayoutDialog)
#: kcm_add_layout_dialog.ui:14
#, kde-format
msgid "Add Layout"
msgstr "Indeling toevoegen"

#. i18n: ectx: property (placeholderText), widget (QLineEdit, layoutSearchField)
#: kcm_add_layout_dialog.ui:20
#, kde-format
msgid "Search…"
msgstr "Zoeken…"

#. i18n: ectx: property (text), widget (QLabel, shortcutLabel)
#: kcm_add_layout_dialog.ui:45
#, kde-format
msgid "Shortcut:"
msgstr "Sneltoets:"

#. i18n: ectx: property (text), widget (QLabel, labelLabel)
#: kcm_add_layout_dialog.ui:55
#, kde-format
msgid "Label:"
msgstr "Label:"

#. i18n: ectx: property (text), widget (QPushButton, prevbutton)
#. i18n: ectx: property (text), widget (QPushButton, previewButton)
#: kcm_add_layout_dialog.ui:76 kcm_keyboard.ui:315
#, kde-format
msgid "Preview"
msgstr "Voorbeeld"

#. i18n: ectx: attribute (title), widget (QWidget, tabHardware)
#: kcm_keyboard.ui:18
#, kde-format
msgid "Hardware"
msgstr "Hardware"

#. i18n: ectx: property (text), widget (QLabel, label_4)
#: kcm_keyboard.ui:33
#, kde-format
msgid "Keyboard &model:"
msgstr "Toetsenbord&model:"

#. i18n: ectx: property (whatsThis), widget (QComboBox, keyboardModelComboBox)
#: kcm_keyboard.ui:53
#, kde-format
msgid ""
"Here you can choose a keyboard model. This setting is independent of your "
"keyboard layout and refers to the \"hardware\" model, i.e. the way your "
"keyboard is manufactured. Modern keyboards that come with your computer "
"usually have two extra keys and are referred to as \"104-key\" models, which "
"is probably what you want if you do not know what kind of keyboard you "
"have.\n"
msgstr ""
"Hier kunt u een toetsenbordmodel uitkiezen. Deze instelling is onafhankelijk "
"van uw toetsenbordindeling en refereert aan het \"hardwaremodel\", oftewel "
"de wijze waarop uw toetsenbord is gemaakt. Moderne toetsenborden die met "
"computers worden meegeleverd hebben twee extra toetsen en worden \"104-key\" "
"modellen genoemd. Als u niet weet welk model u hebt, dan is dit "
"waarschijnlijk het juiste type.\n"

#. i18n: ectx: attribute (title), widget (QWidget, tabLayouts)
#: kcm_keyboard.ui:94
#, kde-format
msgid "Layouts"
msgstr "Indelingen"

#. i18n: ectx: property (whatsThis), widget (QGroupBox, switchingPolicyGroupBox)
#: kcm_keyboard.ui:102
#, kde-format
msgid ""
"If you select \"Application\" or \"Window\" switching policy, changing the "
"keyboard layout will only affect the current application or window."
msgstr ""
"Als u wisselbeleid \"Toepassing\" of \"Venster\" kiest zal het wisselen van "
"indeling alleen effect hebben binnen de huidige toepassing of venster."

#. i18n: ectx: property (title), widget (QGroupBox, switchingPolicyGroupBox)
#: kcm_keyboard.ui:105
#, kde-format
msgid "Switching Policy"
msgstr "Wisselbeleid"

#. i18n: ectx: property (text), widget (QRadioButton, switchByGlobalRadioBtn)
#: kcm_keyboard.ui:111
#, kde-format
msgid "&Global"
msgstr "&Globaal"

#. i18n: ectx: property (text), widget (QRadioButton, switchByDesktopRadioBtn)
#: kcm_keyboard.ui:124
#, kde-format
msgid "&Desktop"
msgstr "&Bureaublad"

#. i18n: ectx: property (text), widget (QRadioButton, switchByApplicationRadioBtn)
#: kcm_keyboard.ui:134
#, kde-format
msgid "&Application"
msgstr "Toep&assing"

#. i18n: ectx: property (text), widget (QRadioButton, switchByWindowRadioBtn)
#: kcm_keyboard.ui:144
#, kde-format
msgid "&Window"
msgstr "&Venster"

#. i18n: ectx: property (title), widget (QGroupBox, shortcutsGroupBox)
#: kcm_keyboard.ui:157
#, kde-format
msgid "Shortcuts for Switching Layout"
msgstr "Sneltoetsen voor wisselen van indeling"

#. i18n: ectx: property (text), widget (QLabel, label)
#: kcm_keyboard.ui:163
#, kde-format
msgid "Main shortcuts:"
msgstr "Hoofdsneltoetsen:"

#. i18n: ectx: property (whatsThis), widget (QPushButton, xkbGrpShortcutBtn)
#: kcm_keyboard.ui:176
#, kde-format
msgid ""
"This is a shortcut for switching layouts which is handled by X.org. It "
"allows modifier-only shortcuts."
msgstr ""
"Dit is een sneltoets voor het wisselen van de toetsenbordindeling. Deze "
"actie wordt afgehandeld door X.org. U kunt ook enkel een modificatietoets "
"als sneltoets instellen."

#. i18n: ectx: property (text), widget (QPushButton, xkbGrpShortcutBtn)
#. i18n: ectx: property (text), widget (QPushButton, xkb3rdLevelShortcutBtn)
#: kcm_keyboard.ui:179 kcm_keyboard.ui:209
#, kde-format
msgctxt "no shortcut defined"
msgid "None"
msgstr "Geen"

#. i18n: ectx: property (text), widget (QToolButton, xkbGrpClearBtn)
#. i18n: ectx: property (text), widget (QToolButton, xkb3rdLevelClearBtn)
#: kcm_keyboard.ui:186 kcm_keyboard.ui:216
#, kde-format
msgid "…"
msgstr "…"

#. i18n: ectx: property (text), widget (QLabel, label_3)
#: kcm_keyboard.ui:193
#, kde-format
msgid "3rd level shortcuts:"
msgstr "3de niveau sneltoetsen:"

#. i18n: ectx: property (whatsThis), widget (QPushButton, xkb3rdLevelShortcutBtn)
#: kcm_keyboard.ui:206
#, kde-format
msgid ""
"This is a shortcut for switching to a third level of the active layout (if "
"it has one) which is handled by X.org. It allows modifier-only shortcuts."
msgstr ""
"Dit is een sneltoets voor het schakelen naar het derde niveau van de actieve "
"toetsenbordindeling (indien aanwezig). Dit wordt afgehandeld door X.org. U "
"kunt ook enkel een modificatietoets instellen."

#. i18n: ectx: property (text), widget (QLabel, label_2)
#: kcm_keyboard.ui:223
#, kde-format
msgid "Alternative shortcut:"
msgstr "Alternatieve sneltoets:"

#. i18n: ectx: property (whatsThis), widget (KKeySequenceWidget, kdeKeySequence)
#: kcm_keyboard.ui:236
#, kde-format
msgid ""
"This is a shortcut for switching layouts. It does not support modifier-only "
"shortcuts and also may not work in some situations (e.g. if popup is active "
"or from screensaver)."
msgstr ""
"Dit is de toetsencombinatie voor het wisselen van de toetsenbordindeling. U "
"kunt niet enkel een modificatietoets instellen en deze toetsencombinatie "
"werkt mogelijk niet in bepaalde situaties (bijv. als een dialoogvenster of "
"de schermbeveiliging actief is)."

#. i18n: ectx: property (title), widget (QGroupBox, kcfg_configureLayouts)
#: kcm_keyboard.ui:261
#, kde-format
msgid "Configure layouts"
msgstr "Indelingen instellen"

#. i18n: ectx: property (text), widget (QPushButton, addLayoutBtn)
#: kcm_keyboard.ui:275
#, kde-format
msgid "Add"
msgstr "Toevoegen"

#. i18n: ectx: property (text), widget (QPushButton, removeLayoutBtn)
#: kcm_keyboard.ui:285
#, kde-format
msgid "Remove"
msgstr "Verwijderen"

#. i18n: ectx: property (text), widget (QPushButton, moveUpBtn)
#: kcm_keyboard.ui:295
#, kde-format
msgid "Move Up"
msgstr "Omhoog"

#. i18n: ectx: property (text), widget (QPushButton, moveDownBtn)
#: kcm_keyboard.ui:305
#, kde-format
msgid "Move Down"
msgstr "Omlaag"

#. i18n: ectx: property (text), widget (QCheckBox, layoutLoopingCheckBox)
#: kcm_keyboard.ui:350
#, kde-format
msgid "Spare layouts"
msgstr "Reserve-indelingen"

#. i18n: ectx: property (text), widget (QLabel, label_5)
#: kcm_keyboard.ui:382
#, kde-format
msgid "Main layout count:"
msgstr "Aantal hoofdindelingen:"

#. i18n: ectx: attribute (title), widget (QWidget, tabAdvanced)
#: kcm_keyboard.ui:412
#, kde-format
msgid "Advanced"
msgstr "Geavanceerd"

#. i18n: ectx: property (text), widget (QCheckBox, kcfg_resetOldXkbOptions)
#: kcm_keyboard.ui:418
#, kde-format
msgid "&Configure keyboard options"
msgstr "Toetsenbord-opties &instellen"

#: kcm_keyboard_widget.cpp:211
#, kde-format
msgctxt "unknown keyboard model vendor"
msgid "Unknown"
msgstr "Onbekend"

#: kcm_keyboard_widget.cpp:213
#, kde-format
msgctxt "vendor | keyboard model"
msgid "%1 | %2"
msgstr "%1 | %2"

#: kcm_keyboard_widget.cpp:643
#, kde-format
msgctxt "no shortcuts defined"
msgid "None"
msgstr "Geen"

#: kcm_keyboard_widget.cpp:657
#, kde-format
msgid "%1 shortcut"
msgid_plural "%1 shortcuts"
msgstr[0] "%1 sneltoets"
msgstr[1] "%1 sneltoetsen"

#: kcm_view_models.cpp:200
#, kde-format
msgctxt "layout map name"
msgid "Map"
msgstr "Naam indeling"

#: kcm_view_models.cpp:200
#, kde-format
msgid "Layout"
msgstr "Indeling"

#: kcm_view_models.cpp:200
#, kde-format
msgid "Variant"
msgstr "Variant"

#: kcm_view_models.cpp:200
#, kde-format
msgid "Label"
msgstr "Label"

#: kcm_view_models.cpp:200
#, kde-format
msgid "Shortcut"
msgstr "Sneltoets"

#: kcm_view_models.cpp:273
#, kde-format
msgctxt "variant"
msgid "Default"
msgstr "Standaard"

#. i18n: ectx: property (text), widget (QLabel, label_2)
#: kcmmiscwidget.ui:31
#, kde-format
msgid "When a key is held:"
msgstr "Wanneer een toets blijft ingedrukt:"

#. i18n: ectx: property (text), widget (QRadioButton, accentMenuRadioButton)
#: kcmmiscwidget.ui:38
#, kde-format
msgid "&Show accented and similar characters "
msgstr "Tekens met accenten en soortgelijke &tonen "

#. i18n: ectx: property (text), widget (QRadioButton, repeatRadioButton)
#: kcmmiscwidget.ui:45
#, kde-format
msgid "&Repeat the key"
msgstr "De toets he&rhalen"

#. i18n: ectx: property (text), widget (QRadioButton, nothingRadioButton)
#: kcmmiscwidget.ui:52
#, kde-format
msgid "&Do nothing"
msgstr "Niets &doen"

#. i18n: ectx: property (text), widget (QLabel, label)
#: kcmmiscwidget.ui:66
#, kde-format
msgid "Test area:"
msgstr "Testgebied:"

#. i18n: ectx: property (toolTip), widget (QLineEdit, lineEdit)
#: kcmmiscwidget.ui:73
#, kde-format
msgid ""
"Allows to test keyboard repeat and click volume (just don't forget to apply "
"the changes)."
msgstr ""
"Sta het testen van de herhaalfunctie van het toetsenbord en het volume van "
"het klikken toe (vergeet niet om de wijzigingen toe te passen)"

#. i18n: ectx: property (whatsThis), widget (QGroupBox, numlockGroupBox)
#: kcmmiscwidget.ui:82
#, kde-format
msgid ""
"If supported, this option allows you to setup the state of NumLock after "
"Plasma startup.<p>You can configure NumLock to be turned on or off, or "
"configure Plasma not to set NumLock state."
msgstr ""
"Indien ondersteund, kunt u met deze optie de status van het Numerieke "
"toetsenbord (NumLock) na het opstarten van Plasma bepalen. <p>U kunt bepalen "
"of Numlock aan of uit gezet wordt, of dat Plasma de Numlock ongemoeid dient "
"te laten."

#. i18n: ectx: property (title), widget (QGroupBox, numlockGroupBox)
#: kcmmiscwidget.ui:85
#, kde-format
msgid "NumLock on Plasma Startup"
msgstr "NumLock bij Plasma-start"

#. i18n: ectx: property (text), widget (QRadioButton, radioButton1)
#: kcmmiscwidget.ui:97
#, kde-format
msgid "T&urn on"
msgstr "Ins&chakelen"

#. i18n: ectx: property (text), widget (QRadioButton, radioButton2)
#: kcmmiscwidget.ui:104
#, kde-format
msgid "&Turn off"
msgstr "&Uitschakelen"

#. i18n: ectx: property (text), widget (QRadioButton, radioButton3)
#: kcmmiscwidget.ui:111
#, kde-format
msgid "Leave unchan&ged"
msgstr "&Ongewijzigd laten"

#. i18n: ectx: property (text), widget (QLabel, lblRate)
#: kcmmiscwidget.ui:148
#, kde-format
msgid "&Rate:"
msgstr "He&rhalingstempo:"

#. i18n: ectx: property (whatsThis), widget (QSlider, delaySlider)
#. i18n: ectx: property (whatsThis), widget (QSpinBox, kcfg_repeatDelay)
#: kcmmiscwidget.ui:164 kcmmiscwidget.ui:202
#, kde-format
msgid ""
"If supported, this option allows you to set the delay after which a pressed "
"key will start generating keycodes. The 'Repeat rate' option controls the "
"frequency of these keycodes."
msgstr ""
"Indien van toepassing kunt u met deze instelling de vertraging bepalen "
"waarna de ingedrukte toets begint met het genereren van toetsencodes. Het "
"herhalingstempo bepaalt de frequentie van deze toetsencodes."

#. i18n: ectx: property (whatsThis), widget (QDoubleSpinBox, kcfg_repeatRate)
#. i18n: ectx: property (whatsThis), widget (QSlider, rateSlider)
#: kcmmiscwidget.ui:192 kcmmiscwidget.ui:212
#, kde-format
msgid ""
"If supported, this option allows you to set the rate at which keycodes are "
"generated while a key is pressed."
msgstr ""
"Indien van toepassing kunt u met deze optie bepalen hoeveel toetsencodes er "
"worden gegenereerd als een u een toets ingedrukt houdt."

#. i18n: ectx: property (suffix), widget (QDoubleSpinBox, kcfg_repeatRate)
#: kcmmiscwidget.ui:195
#, kde-format
msgid " repeats/s"
msgstr " herhalingen/s"

#. i18n: ectx: property (suffix), widget (QSpinBox, kcfg_repeatDelay)
#: kcmmiscwidget.ui:205
#, kde-format
msgid " ms"
msgstr " ms"

#. i18n: ectx: property (text), widget (QLabel, lblDelay)
#: kcmmiscwidget.ui:246
#, kde-format
msgid "&Delay:"
msgstr "Vertra&ging:"

#: tastenbrett/main.cpp:57
#, kde-format
msgctxt "app display name"
msgid "Keyboard Preview"
msgstr "Voorvertoning toetsenbord"

#: tastenbrett/main.cpp:59
#, kde-format
msgctxt "app description"
msgid "Keyboard layout visualization"
msgstr "Visualisatie van toetsenbordindeling"

#: tastenbrett/main.cpp:144
#, kde-format
msgctxt "@label"
msgid ""
"The keyboard geometry failed to load. This often indicates that the selected "
"model does not support a specific layout or layout variant. This problem "
"will likely also present when you try to use this combination of model, "
"layout and variant."
msgstr ""
"Het laden van de geometrie van het toetsenbord is mislukt. Dit geeft vaak "
"aan dat het geselecteerde model een specifieke indeling of variant van een "
"indeling niet ondersteunt. Dit probleem zal waarschijnlijk ook aanwezig zijn "
"wanneer u deze combinatie van model, indeling en variant probeert te "
"gebruiken."

#~ msgid "KDE Keyboard Control Module"
#~ msgstr "KDE-Toetsenbordbesturingsprogramma"

#~ msgid "(c) 2010 Andriy Rysin"
#~ msgstr "(c) 2010 Andriy Rysin"

#~ msgid ""
#~ "<h1>Keyboard</h1> This control module can be used to configure keyboard "
#~ "parameters and layouts."
#~ msgstr ""
#~ "<h1>Toetsenbord</h1> Deze besturingsmodule kan worden gebruikt om "
#~ "toetsenbordparameters en indelingen in te stellen."

#~ msgid "KDE Keyboard Layout Switcher"
#~ msgstr "Omschakelaar van toetsenbordindeling van KDE"

#~ msgid "Only up to %1 keyboard layout is supported"
#~ msgid_plural "Only up to %1 keyboard layouts are supported"
#~ msgstr[0] "Slechts %1 toetsenbordindeling wordt ondersteund"
#~ msgstr[1] "Slechts %1 toetsenbordindelingen worden ondersteund"

#~ msgid "Any language"
#~ msgstr "Elke taal"

#~ msgid "Layout:"
#~ msgstr "Indeling:"

#~ msgid "Variant:"
#~ msgstr "Variant:"

#~ msgid "Limit selection by language:"
#~ msgstr "Selectie beperken door de taal:"

#~ msgid "..."
#~ msgstr "..."

#~ msgctxt "short layout label - full layout name"
#~ msgid "%1 - %2"
#~ msgstr "%1 - %2"

#~ msgid "Layout Indicator"
#~ msgstr "Indelingsaanduiding"

#~ msgid "Show layout indicator"
#~ msgstr "Indelingsaanduiding tonen"

#~ msgid "Show for single layout"
#~ msgstr "Enkele indeling tonen"

#~ msgid "Show flag"
#~ msgstr "Vlag tonen"

#~ msgid "Show label"
#~ msgstr "Label tonen"

#~ msgid "Show label on flag"
#~ msgstr "Label op vlag tonen"

#~ msgctxt "tooltip title"
#~ msgid "Keyboard Layout"
#~ msgstr "Toetsenbordindeling"

#~ msgid "Configure Layouts..."
#~ msgstr "Indelingen instellen..."

#~ msgid "Keyboard Repeat"
#~ msgstr "Toetsenbordherhaling"

#~ msgid "Turn o&ff"
#~ msgstr "&Uitschakelen"

#~ msgid "&Leave unchanged"
#~ msgstr "&Ongewijzigd laten"

#~ msgid "Configure..."
#~ msgstr "Instellen..."

#~ msgid "Key Click"
#~ msgstr "Toetsenklik"

#~ msgid ""
#~ "If supported, this option allows you to hear audible clicks from your "
#~ "computer's speakers when you press the keys on your keyboard. This might "
#~ "be useful if your keyboard does not have mechanical keys, or if the sound "
#~ "that the keys make is very soft.<p>You can change the loudness of the key "
#~ "click feedback by dragging the slider button or by clicking the up/down "
#~ "arrows on the spin box. Setting the volume to 0% turns off the key click."
#~ msgstr ""
#~ "Indien ondersteund, zorgt deze optie er voor dat u klikken hoort uit de "
#~ "speakers van uw computer, telkens als u een toets indrukt. Dit kan handig "
#~ "zijn als u een toetsenbord gebruikt dat geen mechanische toetsen heeft, "
#~ "of als het klikgeluid dat de toetsen zelf produceren erg zacht is.<p>U "
#~ "kunt het volume van de toetsenkliks instellen door de schuifregelaar te "
#~ "verslepen, of door op de op/neer pijltjes te klikken. U schakelt het "
#~ "toetsenklikgeluid uit door het volume op 0% te zetten."

#~ msgid "&Key click volume:"
#~ msgstr "&Toetsenklikvolume:"

#~ msgid "No layout selected "
#~ msgstr "Geen indeling geselecteerd "

#~ msgid "XKB extension failed to initialize"
#~ msgstr "Initialisatie van XKB-extensie is mislukt"

#~ msgid "Backspace"
#~ msgstr "Backspace"

#~ msgctxt "Tab key"
#~ msgid "Tab"
#~ msgstr "Tab"

#~ msgid "Caps Lock"
#~ msgstr "Caps Lock"

#~ msgid "Enter"
#~ msgstr "Enter"

#~ msgid "Ctrl"
#~ msgstr "Ctrl"

#~ msgid "Alt"
#~ msgstr "Alt"

#~ msgid "AltGr"
#~ msgstr "AltGr"

#~ msgid "Esc"
#~ msgstr "Esc"

#~ msgctxt "Function key"
#~ msgid "F%1"
#~ msgstr "F%1"

#~ msgid "Shift"
#~ msgstr "Shift"

#~ msgid "No preview found"
#~ msgstr "Geen voorbeeld gevonden"

#~ msgid "Close"
#~ msgstr "Sluiten"

#~ msgid ""
#~ "If you check this option, pressing and holding down a key emits the same "
#~ "character over and over again. For example, pressing and holding down the "
#~ "Tab key will have the same effect as that of pressing that key several "
#~ "times in succession: Tab characters continue to be emitted until you "
#~ "release the key."
#~ msgstr ""
#~ "Als u deze optie inschakelt, dan wordt een teken herhaaldelijk achter "
#~ "elkaar ingevoegd, wanneer u een toets ingedrukt houdt. Bijv. als u de "
#~ "toets TAB ingedrukt houdt, dan heeft dit hetzelfde effect als wanneer u "
#~ "enkele malen achter elkaar op de toets drukt. De tabs worden continue "
#~ "ingevoegd, totdat u de toets weer los laat."

#~ msgid "&Enable keyboard repeat"
#~ msgstr "Toetsenbordh&erhaling inschakelen"
