# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the plasma-desktop package.
#
# A S Alam <aalam.yellow@gmail.com>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: plasma-desktop\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-04-12 02:13+0000\n"
"PO-Revision-Date: 2021-10-09 09:15-0700\n"
"Last-Translator: A S Alam <aalam@satluj.org>\n"
"Language-Team: Punjabi <punjabi-users@lists.sf.net>\n"
"Language: pa\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 21.04.3\n"

#: app/emojier.cpp:84 app/emojier.cpp:86
#, kde-format
msgid "Emoji Selector"
msgstr "ਇਮੋਜ਼ੀ ਚੋਣਕਾਰ"

#: app/emojier.cpp:88
#, kde-format
msgid "(C) 2019 Aleix Pol i Gonzalez"
msgstr "(C) 2019 Aleix Pol i Gonzalez"

#: app/emojier.cpp:90
#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "ਅਮਨਪਰੀਤ ਸਿੰਘ ਆਲਮ ੨੦੨੧"

#: app/emojier.cpp:90
#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "alam.yellow@gmail.com"

#: app/emojier.cpp:105
#, kde-format
msgid "Replace an existing instance"
msgstr "ਮੌਜੂਦਾ ਮੌਕੇ ਨੂੰ ਬਦਲੋ"

#: app/ui/CategoryPage.qml:33 app/ui/emojier.qml:50
#, kde-format
msgid "Search"
msgstr "ਖੋਜੋ"

#: app/ui/CategoryPage.qml:80
#, kde-format
msgid "Clear History"
msgstr "ਅਤੀਤ ਨੂੰ ਮਿਟਾਓ"

#: app/ui/CategoryPage.qml:104
#, kde-format
msgctxt "@item:inmenu"
msgid "Copy Character"
msgstr ""

#: app/ui/CategoryPage.qml:107 app/ui/CategoryPage.qml:115
#: app/ui/emojier.qml:34
#, kde-format
msgid "%1 copied to the clipboard"
msgstr "%1 ਨੂੰ ਕਲਿੱਪਬੋਰਡ ਵਿੱਚ ਕਾਪੀ ਕੀਤਾ"

#: app/ui/CategoryPage.qml:112
#, kde-format
msgctxt "@item:inmenu"
msgid "Copy Description"
msgstr ""

#: app/ui/CategoryPage.qml:205
#, kde-format
msgid "No recent Emojis"
msgstr ""

#: app/ui/emojier.qml:40
#, kde-format
msgid "Recent"
msgstr "ਤਾਜ਼ਾ"

#: app/ui/emojier.qml:61
#, kde-format
msgid "All"
msgstr "ਸਭ"

#: app/ui/emojier.qml:72
#, kde-format
msgid "Categories"
msgstr "ਕੈਟਾਗਰੀਆਂ"

#: emojicategory.cpp:14
msgctxt "Emoji Category"
msgid "Smileys and Emotion"
msgstr ""

#: emojicategory.cpp:15
msgctxt "Emoji Category"
msgid "People and Body"
msgstr ""

#: emojicategory.cpp:16
msgctxt "Emoji Category"
msgid "Component"
msgstr ""

#: emojicategory.cpp:17
msgctxt "Emoji Category"
msgid "Animals and Nature"
msgstr ""

#: emojicategory.cpp:18
msgctxt "Emoji Category"
msgid "Food and Drink"
msgstr ""

#: emojicategory.cpp:19
msgctxt "Emoji Category"
msgid "Travel and Places"
msgstr ""

#: emojicategory.cpp:20
msgctxt "Emoji Category"
msgid "Activities"
msgstr ""

#: emojicategory.cpp:21
msgctxt "Emoji Category"
msgid "Objects"
msgstr ""

#: emojicategory.cpp:22
msgctxt "Emoji Category"
msgid "Symbols"
msgstr ""

#: emojicategory.cpp:23
msgctxt "Emoji Category"
msgid "Flags"
msgstr ""

#~ msgid "Search…"
#~ msgstr "…ਖੋਜੋ"

#~ msgid "Search..."
#~ msgstr "ਖੋਜੋ..."
