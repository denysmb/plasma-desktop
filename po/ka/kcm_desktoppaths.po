# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the plasma-desktop package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: plasma-desktop\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-04-08 02:19+0000\n"
"PO-Revision-Date: 2023-02-13 07:05+0100\n"
"Last-Translator: Temuri Doghonadze <temuri.doghonadze@gmail.com>\n"
"Language-Team: Georgian <kde-i18n-doc@kde.org>\n"
"Language: ka\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Poedit 3.2.2\n"

#: desktoppathssettings.cpp:211
#, kde-format
msgid "Desktop"
msgstr "სამუშაო მაგიდა"

#: desktoppathssettings.cpp:226
#, kde-format
msgid "Documents"
msgstr "დოკუმენტები"

#: desktoppathssettings.cpp:241
#, kde-format
msgid "Downloads"
msgstr "გადმოწერები"

#: desktoppathssettings.cpp:256
#, kde-format
msgid "Music"
msgstr "მუსიკა"

#: desktoppathssettings.cpp:271
#, kde-format
msgid "Pictures"
msgstr "სურათები"

#: desktoppathssettings.cpp:286
#, kde-format
msgid "Videos"
msgstr "ვიდეო"

#: desktoppathssettings.cpp:301
#, kde-format
msgid "Public"
msgstr "საჯარო"

#: desktoppathssettings.cpp:316
#, kde-format
msgid "Templates"
msgstr "ნიმუშები"

#: package/contents/ui/main.qml:23
#, kde-format
msgid "Desktop path:"
msgstr "სამუშაო მაგიდის ბილიკი:"

#: package/contents/ui/main.qml:26
#, kde-format
msgid ""
"This folder contains all the files which you see on your desktop. You can "
"change the location of this folder if you want to, and the contents will "
"move automatically to the new location as well."
msgstr ""
"ეს საქაღალდე შეიცავს ყველა ფაილს, რომელსაც ხედავთ თქვენს სამუშაო მაგიდაზე. "
"თუ გსურთ, შეგიძლიათ შეცვალოთ ამ საქაღალდის მდებარეობა და შიგთავსი "
"ავტომატურად გადავა ახალ ადგილას."

#: package/contents/ui/main.qml:32
#, kde-format
msgid "Documents path:"
msgstr "დოკუმენტების ბილიკი:"

#: package/contents/ui/main.qml:35
#, kde-format
msgid ""
"This folder will be used by default to load or save documents from or to."
msgstr ""
"ეს საქაღალდე ნაგულისხმევად გამოყენებული იქნება დოკუმენტების ჩასატვირთად ან "
"შესანახად."

#: package/contents/ui/main.qml:41
#, kde-format
msgid "Downloads path:"
msgstr "გადმოწერების ბილიკი:"

#: package/contents/ui/main.qml:44
#, kde-format
msgid "This folder will be used by default to save your downloaded items."
msgstr ""
"ეს საქაღალდე ნაგულისხმებად გამოყენებული იქნება თქვენი გადმოწერილი ფაილების "
"შესანახად."

#: package/contents/ui/main.qml:50
#, kde-format
msgid "Videos path:"
msgstr "ვიდეოების ბილიკი:"

#: package/contents/ui/main.qml:53 package/contents/ui/main.qml:80
#, kde-format
msgid "This folder will be used by default to load or save movies from or to."
msgstr ""
"ეს საქაღალდე ნაგულისხმებად გამოყენებული იქნება ფილმების ჩასატვირთად ან "
"შესანახად."

#: package/contents/ui/main.qml:59
#, kde-format
msgid "Pictures path:"
msgstr "სურათების ბილიკი:"

#: package/contents/ui/main.qml:62
#, kde-format
msgid ""
"This folder will be used by default to load or save pictures from or to."
msgstr ""
"ეს საქაღალდე ნაგულისხმევად გამოყენებული იქნება სურათების ჩასატვირთად ან "
"შესანახად."

#: package/contents/ui/main.qml:68
#, kde-format
msgid "Music path:"
msgstr "მუსიკის ბილიკი:"

#: package/contents/ui/main.qml:71
#, kde-format
msgid "This folder will be used by default to load or save music from or to."
msgstr ""
"ეს საქაღალდე ნაგულისხმებად გამოყენებული იქნება მუსიკის ჩასატვირთად ან "
"შესანახად."

#: package/contents/ui/main.qml:77
#, kde-format
msgid "Public path:"
msgstr "საჯარო ბილიკი:"

#: package/contents/ui/main.qml:86
#, kde-format
msgid "Templates path:"
msgstr "შაბლონების ბილიკი:"

#: package/contents/ui/main.qml:89
#, kde-format
msgid ""
"This folder will be used by default to load or save templates from or to."
msgstr ""
"ეს საქაღალდე ნაგულისხმევად გამოყენებული იქნება შაბლონების ჩასატვირთად ან "
"შესანახად."

#: package/contents/ui/UrlRequester.qml:65
#, kde-format
msgctxt "@action:button"
msgid "Choose new location"
msgstr "აირჩიეთ ახალი მდებარეობა"

#~ msgid ""
#~ "<h1>Paths</h1>\n"
#~ "This module allows you to choose where in the filesystem the files on "
#~ "your desktop should be stored.\n"
#~ "Use the \"Whats This?\" (Shift+F1) to get help on specific options."
#~ msgstr ""
#~ "<h1>ბილიკები</h1>\n"
#~ "ეს მოდული გაძლევთ საშუალებას აირჩიოთ ფაილურ სისტემაში სად უნდა იყოს "
#~ "შენახული ფაილები თქვენს სამუშაო მაგიდაზე.\n"
#~ "კონკრეტულ პარამეტრებზე დახმარების მისაღებად გამოიყენეთ \"ეს რა არის?"
#~ "\" (Shift+F1)."

#~ msgid ""
#~ "This folder will be used by default to load or save public shares from or "
#~ "to."
#~ msgstr ""
#~ "ეს საქაღალდე ნაგულისხმევად გამოყენებული იქნება საჯარო გაზიარებულების "
#~ "ჩასატვირთად ან შესანახად."
