# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
# Tommi Nieminen <translator@legisign.org>, 2017, 2018, 2019, 2020, 2021, 2022, 2023.
# Lasse Liehu <lasse.liehu@iki.fi>, 2022.
#
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-04-14 02:04+0000\n"
"PO-Revision-Date: 2023-03-04 19:58+0200\n"
"Last-Translator: Tommi Nieminen <translator@legisign.org>\n"
"Language-Team: Finnish <kde-i18n-doc@kde.org>\n"
"Language: fi\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 22.12.3\n"

#: package/contents/config/config.qml:13
#, kde-format
msgid "Appearance"
msgstr "Ulkoasu"

#: package/contents/config/config.qml:18
#, kde-format
msgid "Behavior"
msgstr "Toiminta"

#: package/contents/ui/AudioStream.qml:101
#, kde-format
msgctxt "@action:button"
msgid "Unmute"
msgstr "Poista vaimennus"

#: package/contents/ui/AudioStream.qml:101
#, kde-format
msgctxt "@action:button"
msgid "Mute"
msgstr "Vaimenna"

#: package/contents/ui/AudioStream.qml:102
#, kde-format
msgctxt "@info:tooltip %1 is the window title"
msgid "Unmute %1"
msgstr "Poista vaimennus: %1"

# Kaksoispiste yhdenmukaisuussyistä: ”Poista vaimennus” vaatii genetiivin (poista JONKIN vaimennus)
#: package/contents/ui/AudioStream.qml:102
#, kde-format
msgctxt "@info:tooltip %1 is the window title"
msgid "Mute %1"
msgstr "Vaimenna: %1"

#: package/contents/ui/Badge.qml:54
#, kde-format
msgctxt "Invalid number of new messages, overlay, keep short"
msgid "—"
msgstr "—"

#: package/contents/ui/Badge.qml:56
#, kde-format
msgctxt "Over 9999 new messages, overlay, keep short"
msgid "9,999+"
msgstr "9,999+"

#: package/contents/ui/ConfigAppearance.qml:33
#, kde-format
msgid "General:"
msgstr "Yleistä:"

#: package/contents/ui/ConfigAppearance.qml:34
#, kde-format
msgid "Show small window previews when hovering over Tasks"
msgstr "Näytä tehtävän yllä oltaessa pieni ikkunaesikatselu"

#: package/contents/ui/ConfigAppearance.qml:39
#, kde-format
msgid "Hide other windows when hovering over previews"
msgstr "Piilota muut ikkunat esikatselujen yllä ollessa"

#: package/contents/ui/ConfigAppearance.qml:44
#, kde-format
msgid "Mark applications that play audio"
msgstr "Osoita ääntä toistavat sovellukset"

#: package/contents/ui/ConfigAppearance.qml:52
#, kde-format
msgctxt "@option:check"
msgid "Fill free space on Panel"
msgstr "Täytä tyhjä tila paneelissa"

#: package/contents/ui/ConfigAppearance.qml:61
#, kde-format
msgid "Maximum columns:"
msgstr "Sarakkeita enintään:"

#: package/contents/ui/ConfigAppearance.qml:61
#, kde-format
msgid "Maximum rows:"
msgstr "Rivejä enintään:"

#: package/contents/ui/ConfigAppearance.qml:67
#, kde-format
msgid "Always arrange tasks in rows of as many columns"
msgstr "Asettele tehtävät aina tasasarakkeisiksi riveiksi"

#: package/contents/ui/ConfigAppearance.qml:67
#, kde-format
msgid "Always arrange tasks in columns of as many rows"
msgstr "Asettele tehtävät aina tasarivisiksi sarakkeiksi"

#: package/contents/ui/ConfigAppearance.qml:77
#, kde-format
msgid "Spacing between icons:"
msgstr "Kuvakevälistys:"

#: package/contents/ui/ConfigAppearance.qml:81
#, kde-format
msgctxt "@item:inlistbox Icon spacing"
msgid "Small"
msgstr "Pieni"

#: package/contents/ui/ConfigAppearance.qml:85
#, kde-format
msgctxt "@item:inlistbox Icon spacing"
msgid "Normal"
msgstr "Tavallinen"

#: package/contents/ui/ConfigAppearance.qml:89
#, kde-format
msgctxt "@item:inlistbox Icon spacing"
msgid "Large"
msgstr "Suuri"

#: package/contents/ui/ConfigAppearance.qml:113
#, kde-format
msgctxt "@info:usagetip under a set of radio buttons when Touch Mode is on"
msgid "Automatically set to Large when in Touch Mode"
msgstr "Aseta automaattisesti suureksi kosketustilassa"

#: package/contents/ui/ConfigBehavior.qml:47
#, kde-format
msgid "Group:"
msgstr "Ryhmittely:"

#: package/contents/ui/ConfigBehavior.qml:50
#, kde-format
msgid "Do not group"
msgstr "Älä ryhmittele"

#: package/contents/ui/ConfigBehavior.qml:50
#, kde-format
msgid "By program name"
msgstr "Ohjelman nimen mukaan"

#: package/contents/ui/ConfigBehavior.qml:55
#, kde-format
msgid "Clicking grouped task:"
msgstr "Ryhmitettyjen tehtävien napsautus:"

#: package/contents/ui/ConfigBehavior.qml:62
#, kde-format
msgctxt "Completes the sentence 'Clicking grouped task cycles through tasks' "
msgid "Cycles through tasks"
msgstr "vaihtaa tehtävää"

#: package/contents/ui/ConfigBehavior.qml:63
#, kde-format
msgctxt ""
"Completes the sentence 'Clicking grouped task shows tooltip window "
"thumbnails' "
msgid "Shows small window previews"
msgstr "Näyttää pienet ikkunaesikatselut"

#: package/contents/ui/ConfigBehavior.qml:64
#, kde-format
msgctxt ""
"Completes the sentence 'Clicking grouped task shows windows side by side' "
msgid "Shows large window previews"
msgstr "Näyttää suuret ikkunaesikatselut"

#: package/contents/ui/ConfigBehavior.qml:65
#, kde-format
msgctxt "Completes the sentence 'Clicking grouped task shows textual list' "
msgid "Shows textual list"
msgstr "näyttää tekstimuotoisen luettelon"

#: package/contents/ui/ConfigBehavior.qml:73
#, kde-format
msgid ""
"The compositor does not support displaying windows side by side, so a "
"textual list will be displayed instead."
msgstr ""
"Koostaja ei tue ikkunoiden esittämistä rinnakkain, joten näytetään sen "
"sijaan tekstimuotoinen luettelo."

#: package/contents/ui/ConfigBehavior.qml:83
#, kde-format
msgid "Combine into single button"
msgstr "Yhdistä yhdeksi painikkeeksi"

#: package/contents/ui/ConfigBehavior.qml:90
#, kde-format
msgid "Group only when the Task Manager is full"
msgstr "Ryhmittele vain tehtäväpalkin ollessa täynnä"

#: package/contents/ui/ConfigBehavior.qml:101
#, kde-format
msgid "Sort:"
msgstr "Lajittelu:"

#: package/contents/ui/ConfigBehavior.qml:104
#, kde-format
msgid "Do not sort"
msgstr "Älä lajittele"

#: package/contents/ui/ConfigBehavior.qml:104
#, kde-format
msgid "Manually"
msgstr "Käsin"

#: package/contents/ui/ConfigBehavior.qml:104
#, kde-format
msgid "Alphabetically"
msgstr "Aakkosta"

#: package/contents/ui/ConfigBehavior.qml:104
#, kde-format
msgid "By desktop"
msgstr "Työpöydittäin"

#: package/contents/ui/ConfigBehavior.qml:104
#, kde-format
msgid "By activity"
msgstr "Aktiviteeteittain"

#: package/contents/ui/ConfigBehavior.qml:110
#, kde-format
msgid "Keep launchers separate"
msgstr "Erota käynnistimet"

#: package/contents/ui/ConfigBehavior.qml:121
#, kde-format
msgctxt "Part of a sentence: 'Clicking active task minimizes the task'"
msgid "Clicking active task:"
msgstr "Aktiivisen tehtävän napsautus:"

#: package/contents/ui/ConfigBehavior.qml:122
#, kde-format
msgctxt "Part of a sentence: 'Clicking active task minimizes the task'"
msgid "Minimizes the task"
msgstr "pienentää tehtävän"

#: package/contents/ui/ConfigBehavior.qml:127
#, kde-format
msgid "Middle-clicking any task:"
msgstr "Tehtävän napsautus hiiren keskipainikkeella:"

#: package/contents/ui/ConfigBehavior.qml:131
#, kde-format
msgctxt "Part of a sentence: 'Middle-clicking any task does nothing'"
msgid "Does nothing"
msgstr "ei tee mitään"

#: package/contents/ui/ConfigBehavior.qml:132
#, kde-format
msgctxt "Part of a sentence: 'Middle-clicking any task closes window or group'"
msgid "Closes window or group"
msgstr "sulkee ikkunan tai ryhmän"

#: package/contents/ui/ConfigBehavior.qml:133
#, kde-format
msgctxt "Part of a sentence: 'Middle-clicking any task opens a new window'"
msgid "Opens a new window"
msgstr "avaa uuden ikkunan"

#: package/contents/ui/ConfigBehavior.qml:134
#, kde-format
msgctxt ""
"Part of a sentence: 'Middle-clicking any task minimizes/restores window or "
"group'"
msgid "Minimizes/Restores window or group"
msgstr "pienentää tai palauttaa ikkunan tai ryhmän"

#: package/contents/ui/ConfigBehavior.qml:135
#, kde-format
msgctxt "Part of a sentence: 'Middle-clicking any task toggles grouping'"
msgid "Toggles grouping"
msgstr "vaihtaa ryhmitystilaa"

#: package/contents/ui/ConfigBehavior.qml:136
#, kde-format
msgctxt ""
"Part of a sentence: 'Middle-clicking any task brings it to the current "
"virtual desktop'"
msgid "Brings it to the current virtual desktop"
msgstr "tuo sen nykyiselle virtuaalityöpöydälle"

#: package/contents/ui/ConfigBehavior.qml:146
#, kde-format
msgctxt "Part of a sentence: 'Mouse wheel cycles through tasks'"
msgid "Mouse wheel:"
msgstr "Hiiren rulla:"

#: package/contents/ui/ConfigBehavior.qml:147
#, kde-format
msgctxt "Part of a sentence: 'Mouse wheel cycles through tasks'"
msgid "Cycles through tasks"
msgstr "vaihtaa tehtävien välillä"

#: package/contents/ui/ConfigBehavior.qml:156
#, kde-format
msgid "Skip minimized tasks"
msgstr "Sivuuta pienennetyt tehtävät"

#: package/contents/ui/ConfigBehavior.qml:167
#, kde-format
msgid "Show only tasks:"
msgstr "Näytä tehtävät vain:"

#: package/contents/ui/ConfigBehavior.qml:168
#, kde-format
msgid "From current screen"
msgstr "Nykyiseltä näytöltä"

#: package/contents/ui/ConfigBehavior.qml:173
#, kde-format
msgid "From current desktop"
msgstr "Nykyiseltä työpöydältä"

#: package/contents/ui/ConfigBehavior.qml:178
#, kde-format
msgid "From current activity"
msgstr "Nykyisestä aktiviteetista"

#: package/contents/ui/ConfigBehavior.qml:183
#, kde-format
msgid "That are minimized"
msgstr "Pienennetyt tehtävät"

#: package/contents/ui/ConfigBehavior.qml:192
#, kde-format
msgid "When panel is hidden:"
msgstr "Paneelin ollessa piilossa:"

#: package/contents/ui/ConfigBehavior.qml:193
#, kde-format
msgid "Unhide when a window wants attention"
msgstr "Näytä, kun ikkuna vaatii huomiota"

#: package/contents/ui/ConfigBehavior.qml:205
#, kde-format
msgid "New tasks appear:"
msgstr "Uudet tehtävät ilmestyvät:"

#: package/contents/ui/ConfigBehavior.qml:209
#, kde-format
msgid "On the bottom"
msgstr "Alhaalla"

#: package/contents/ui/ConfigBehavior.qml:213
#: package/contents/ui/ConfigBehavior.qml:233
#, kde-format
msgid "To the right"
msgstr "Oikealle"

#: package/contents/ui/ConfigBehavior.qml:215
#: package/contents/ui/ConfigBehavior.qml:231
#, kde-format
msgid "To the left"
msgstr "Vasemmalle"

#: package/contents/ui/ConfigBehavior.qml:227
#, kde-format
msgid "On the Top"
msgstr "Ylhäällä"

#: package/contents/ui/ContextMenu.qml:93
#, kde-format
msgid "Places"
msgstr "Sijainnit"

#: package/contents/ui/ContextMenu.qml:98
#, kde-format
msgid "Recent Files"
msgstr "Viimeisimmät tiedostot"

#: package/contents/ui/ContextMenu.qml:103
#, kde-format
msgid "Actions"
msgstr "Toiminnot"

#: package/contents/ui/ContextMenu.qml:168
#, kde-format
msgctxt "Play previous track"
msgid "Previous Track"
msgstr "Edellinen kappale"

#: package/contents/ui/ContextMenu.qml:182
#, kde-format
msgctxt "Pause playback"
msgid "Pause"
msgstr "Tauko"

#: package/contents/ui/ContextMenu.qml:182
#, kde-format
msgctxt "Start playback"
msgid "Play"
msgstr "Toista"

#: package/contents/ui/ContextMenu.qml:200
#, kde-format
msgctxt "Play next track"
msgid "Next Track"
msgstr "Seuraava kappale"

#: package/contents/ui/ContextMenu.qml:211
#, kde-format
msgctxt "Stop playback"
msgid "Stop"
msgstr "Pysäytä"

#: package/contents/ui/ContextMenu.qml:231
#, kde-format
msgctxt "Quit media player app"
msgid "Quit"
msgstr "Lopeta"

#: package/contents/ui/ContextMenu.qml:246
#, kde-format
msgctxt "Open or bring to the front window of media player app"
msgid "Restore"
msgstr "Palauta"

#: package/contents/ui/ContextMenu.qml:272
#, kde-format
msgid "Mute"
msgstr "Vaimenna"

#: package/contents/ui/ContextMenu.qml:283
#, kde-format
msgid "Open New Window"
msgstr "Avaa uusi ikkuna"

# *** TARKISTA: Siirrä vai siirry? Sama koskee seuraavaa
#: package/contents/ui/ContextMenu.qml:299
#, kde-format
msgid "Move to &Desktop"
msgstr "Siirrä &työpöydälle"

#: package/contents/ui/ContextMenu.qml:323
#, kde-format
msgid "Move &To Current Desktop"
msgstr "Siirrä &nykyiselle työpöydälle"

#: package/contents/ui/ContextMenu.qml:332
#, kde-format
msgid "&All Desktops"
msgstr "Näytä k&aikilla työpöydillä"

#: package/contents/ui/ContextMenu.qml:346
#, kde-format
msgctxt "1 = number of desktop, 2 = desktop name"
msgid "&%1 %2"
msgstr "&%1 %2"

#: package/contents/ui/ContextMenu.qml:360
#, kde-format
msgid "&New Desktop"
msgstr "&Uusi työpöytä"

#: package/contents/ui/ContextMenu.qml:380
#, kde-format
msgid "Show in &Activities"
msgstr "Näytä &aktiviteeteissa"

#: package/contents/ui/ContextMenu.qml:404
#, kde-format
msgid "Add To Current Activity"
msgstr "Lisää nykyiseen aktiviteettiin"

#: package/contents/ui/ContextMenu.qml:414
#, kde-format
msgid "All Activities"
msgstr "Kaikissa aktiviteeteissa"

# *** TARKISTA: Siirrä vai siirry?
#: package/contents/ui/ContextMenu.qml:472
#, kde-format
msgid "Move to %1"
msgstr "Siirrä kohteeseen %1"

#: package/contents/ui/ContextMenu.qml:500
#: package/contents/ui/ContextMenu.qml:517
#, kde-format
msgid "&Pin to Task Manager"
msgstr "K&iinnitä tehtävienhallintaan"

#: package/contents/ui/ContextMenu.qml:570
#, kde-format
msgid "On All Activities"
msgstr "Kaikkiin aktiviteetteihin"

#: package/contents/ui/ContextMenu.qml:576
#, kde-format
msgid "On The Current Activity"
msgstr "Nykyiseen aktiviteettiin"

#: package/contents/ui/ContextMenu.qml:600
#, kde-format
msgid "Unpin from Task Manager"
msgstr "Poista kiinnitys tehtävienhallinnasta"

#: package/contents/ui/ContextMenu.qml:615
#, kde-format
msgid "More"
msgstr "Lisää"

#: package/contents/ui/ContextMenu.qml:624
#, kde-format
msgid "&Move"
msgstr "&Siirrä"

#: package/contents/ui/ContextMenu.qml:633
#, kde-format
msgid "Re&size"
msgstr "&Muuta kokoa"

#: package/contents/ui/ContextMenu.qml:647
#, kde-format
msgid "Ma&ximize"
msgstr "S&uurenna"

#: package/contents/ui/ContextMenu.qml:661
#, kde-format
msgid "Mi&nimize"
msgstr "P&ienennä"

#: package/contents/ui/ContextMenu.qml:671
#, kde-format
msgid "Keep &Above Others"
msgstr "Pidä &ylinnä"

#: package/contents/ui/ContextMenu.qml:681
#, kde-format
msgid "Keep &Below Others"
msgstr "Pidä &alinna"

#: package/contents/ui/ContextMenu.qml:693
#, kde-format
msgid "&Fullscreen"
msgstr "&Koko näyttö"

#: package/contents/ui/ContextMenu.qml:705
#, kde-format
msgid "&Shade"
msgstr "&Rullaa"

#: package/contents/ui/ContextMenu.qml:721
#, kde-format
msgid "Allow this program to be grouped"
msgstr "Salli tämän ohjelman ryhmittely"

#: package/contents/ui/ContextMenu.qml:769
#, kde-format
msgctxt "@item:inmenu"
msgid "&Close All"
msgstr "Sulje k&aikki"

#: package/contents/ui/ContextMenu.qml:769
#, kde-format
msgid "&Close"
msgstr "&Sulje"

#: package/contents/ui/Task.qml:82
#, kde-format
msgctxt "@info:usagetip %1 application name"
msgid "Launch %1"
msgstr "Käynnistä %1"

#: package/contents/ui/Task.qml:87
#, kde-format
msgctxt "@info:tooltip"
msgid "There is %1 new message."
msgid_plural "There are %1 new messages."
msgstr[0] "%1 uusi viesti."
msgstr[1] "%1 uutta viestiä."

#: package/contents/ui/Task.qml:96
#, kde-format
msgctxt "@info:usagetip %1 task name"
msgid "Show Task tooltip for %1"
msgstr "Näytä tehtävän %1 työkaluvihje"

#: package/contents/ui/Task.qml:102
#, kde-format
msgctxt "@info:usagetip %1 task name"
msgid "Show windows side by side for %1"
msgstr "Näytä tehtävän %1 ikkunat rinnakkain"

#: package/contents/ui/Task.qml:107
#, kde-format
msgctxt "@info:usagetip %1 task name"
msgid "Open textual list of windows for %1"
msgstr "Avaa tehtävän %1 ikkunat luetteloksi"

#: package/contents/ui/Task.qml:111
#, kde-format
msgid "Activate %1"
msgstr "Aktivoi %1"

#: package/contents/ui/ToolTipInstance.qml:348
#, kde-format
msgctxt "button to unmute app"
msgid "Unmute %1"
msgstr "Vaimenna %1"

#: package/contents/ui/ToolTipInstance.qml:349
#, kde-format
msgctxt "button to mute app"
msgid "Mute %1"
msgstr "Vaimenna %1"

#: package/contents/ui/ToolTipInstance.qml:372
#, kde-format
msgctxt "Accessibility data on volume slider"
msgid "Adjust volume for %1"
msgstr "Säädä äänenvoimakkuutta %1"

#: package/contents/ui/ToolTipInstance.qml:388
#, kde-format
msgctxt "volume percentage"
msgid "%1%"
msgstr "%1 %"

#: package/contents/ui/ToolTipInstance.qml:391
#, kde-format
msgctxt "only used for sizing, should be widest possible string"
msgid "100%"
msgstr "100 %"

#: package/contents/ui/ToolTipInstance.qml:415
#, kde-format
msgctxt "Comma-separated list of desktops"
msgid "On %1"
msgstr "Aktiviteet(e)issa %1"

#: package/contents/ui/ToolTipInstance.qml:418
#, kde-format
msgctxt "Comma-separated list of desktops"
msgid "Pinned to all desktops"
msgstr "Näytetään kaikilla työpöydillä"

#: package/contents/ui/ToolTipInstance.qml:429
#, kde-format
msgctxt "Which virtual desktop a window is currently on"
msgid "Available on all activities"
msgstr "Käytettävissä kaikissa aktiviteeteissa"

#: package/contents/ui/ToolTipInstance.qml:451
#, kde-format
msgctxt "Activities a window is currently on (apart from the current one)"
msgid "Also available on %1"
msgstr "Käytettävissä myös aktiviteetissa %1"

#: package/contents/ui/ToolTipInstance.qml:455
#, kde-format
msgctxt "Which activities a window is currently on"
msgid "Available on %1"
msgstr "Käytettävissä aktiviteetissa %1"

#: plugin/backend.cpp:351
#, kde-format
msgctxt "Show all user Places"
msgid "%1 more Place"
msgid_plural "%1 more Places"
msgstr[0] "Vielä %1 kohde"
msgstr[1] "Vielä %1 kohdetta"

#: plugin/backend.cpp:447
#, kde-format
msgid "Recent Downloads"
msgstr "Viimeisimmät lataukset"

#: plugin/backend.cpp:449
#, kde-format
msgid "Recent Connections"
msgstr "Viimeisimmät yhteydet"

#: plugin/backend.cpp:451
#, kde-format
msgid "Recent Places"
msgstr "Viimeisimmät sijainnit"

#: plugin/backend.cpp:460
#, kde-format
msgctxt "@action:inmenu"
msgid "Forget Recent Downloads"
msgstr "Unohda viimeisimmät lataukset"

#: plugin/backend.cpp:462
#, kde-format
msgctxt "@action:inmenu"
msgid "Forget Recent Connections"
msgstr "Unohda viimeisimmät yhteydet"

#: plugin/backend.cpp:464
#, kde-format
msgctxt "@action:inmenu"
msgid "Forget Recent Places"
msgstr "Unohda viimeisimmät sijainnit"

#: plugin/backend.cpp:466
#, kde-format
msgctxt "@action:inmenu"
msgid "Forget Recent Files"
msgstr "Unohda viimeisimmät tiedostot"

#~ msgid ""
#~ "Tooltips are disabled, so the windows will be displayed side by side "
#~ "instead."
#~ msgstr ""
#~ "Työkaluvihjeet on poistettu käytöstä, joten esitetään ikkunat sen sijaan "
#~ "rinnakkain."

#~ msgid ""
#~ "Tooltips are disabled, and the compositor does not support displaying "
#~ "windows side by side, so a textual list will be displayed instead"
#~ msgstr ""
#~ "Työkaluvihjeet on poistettu käytöstä eikä koostaja tue ikkunoiden "
#~ "esittämistä rinnakkain, joten näytetään niiden sijaan tekstimuotoinen "
#~ "luettelo"

#~ msgid "Show tooltips"
#~ msgstr "Näytä työkaluvihjeet"

#~ msgctxt ""
#~ "Completes the sentence 'Clicking grouped task shows tooltip window "
#~ "thumbnails' "
#~ msgid "Shows tooltip window thumbnails"
#~ msgstr "näyttää työkaluvihjeenä ikkunoiden pienoiskuvat"

#~ msgctxt ""
#~ "Completes the sentence 'Clicking grouped task shows 'Present Windows' "
#~ "effect' "
#~ msgid "Shows 'Present Windows' effect"
#~ msgstr "näyttää ”Nykyiset ikkunat” -tehosteen"

#~ msgid "Icon size:"
#~ msgstr "Kuvakekoko:"

#~ msgid "Start New Instance"
#~ msgstr "Aloita uusi instanssi"

#~ msgid "More Actions"
#~ msgstr "Lisää toimintoja"

#~ msgid "Panel Hiding:"
#~ msgstr "Paneelin piilotus:"

#~ msgid "Cycle through tasks"
#~ msgstr "Vaihda tehtävää"

#~ msgid "On middle-click:"
#~ msgstr "Hiiren keskipainikkeella:"

#~ msgctxt "The click action"
#~ msgid "None"
#~ msgstr "Älä tee mitään"

#~ msgctxt "When clicking it would toggle grouping windows of a specific app"
#~ msgid "Group/Ungroup"
#~ msgstr "Ryhmittele tai poista ryhmittely"

#~ msgid "Open groups in popups"
#~ msgstr "Avaa ryhmät ponnahdusikkunoihin"

#~ msgid "Filter:"
#~ msgstr "Suodata:"

#~ msgid "Show only tasks from the current desktop"
#~ msgstr "Näytä vain nykyisen työpöydän tehtävät"

#~ msgid "Show only tasks from the current activity"
#~ msgstr "Näytä vain nykyisen aktiviteetin tehtävät"

#~ msgid "Always arrange tasks in as many rows as columns"
#~ msgstr "Asettele tehtävät aina tasasarakkeisiksi riveiksi"

#~ msgid "Always arrange tasks in as many columns as rows"
#~ msgstr "Asettele tehtävät aina tasarivisiksi sarakkeiksi"

# *** TARKISTA: Myös tässä epävarmaa, siirrä vai siirry
#, fuzzy
#~| msgid "Move To &Activity"
#~ msgid "Move to &Activity"
#~ msgstr "Siirrä &aktiviteettiin"

#~ msgid "Show progress and status information in task buttons"
#~ msgstr "Näytä edistyminen ja tilatiedot tehtäväpainikkeissa"

#~ msgctxt ""
#~ "Toggle action for showing a launcher button while the application is not "
#~ "running"
#~ msgid "&Pin"
#~ msgstr "K&iinnitä"

#~ msgid "&Pin"
#~ msgstr "K&iinnitä"

#~ msgctxt ""
#~ "Remove launcher button for application shown while it is not running"
#~ msgid "Unpin"
#~ msgstr "Poista kiinnitys"

#~ msgid "Arrangement"
#~ msgstr "Asettelu"

#~ msgid "Highlight windows"
#~ msgstr "Korosta ikkunat"

#~ msgid "Grouping and Sorting"
#~ msgstr "Ryhmittely ja lajittelu"

#~ msgid "Do Not Sort"
#~ msgstr "Älä lajittele"

#~ msgctxt "Go to previous song"
#~ msgid "Previous"
#~ msgstr "Edellinen"

#~ msgctxt "Pause player"
#~ msgid "Pause"
#~ msgstr "Tauko"

#~ msgctxt "Start player"
#~ msgid "Play"
#~ msgstr "Toista"

#~ msgctxt "Go to next song"
#~ msgid "Next"
#~ msgstr "Seuraava"

#~ msgctxt "close this window"
#~ msgid "Close"
#~ msgstr "Sulje"

#~ msgid "&Show A Launcher When Not Running"
#~ msgstr "&Näytä käynnistin vaikkei olisi käynnissä"

#~ msgid "Remove Launcher"
#~ msgstr "Poista käynnistin"

#~ msgid "Use launcher icons for running applications"
#~ msgstr "Käytä käynnistinkuvakkeita käynnissä oleville ohjelmille"

#~ msgid "Force row settings"
#~ msgstr "Pakota riviasetukset"
