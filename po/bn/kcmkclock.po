# translation of kcmkclock.po to Bengali
# Deepayan Sarkar <deepayan@bengalinux.org>, 2004.
msgid ""
msgstr ""
"Project-Id-Version: kcmkclock\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-02-27 00:49+0000\n"
"PO-Revision-Date: 2004-05-23 16:14-0500\n"
"Last-Translator: Deepayan Sarkar <deepayan@bengalinux.org>\n"
"Language-Team: Bengali <kde-translation@bengalinux.org>\n"
"Language: bn\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 1.3.1\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "দীপায়ন সরকার"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "deepayan@bengalinux.org"

#. i18n: ectx: attribute (title), widget (QWidget, tab)
#: dateandtime.ui:22
#, kde-format
msgid "Date and Time"
msgstr ""

#. i18n: ectx: property (text), widget (QCheckBox, setDateTimeAuto)
#: dateandtime.ui:30
#, kde-format
msgid "Set date and time &automatically"
msgstr ""

#. i18n: ectx: property (text), widget (QLabel, timeServerLabel)
#: dateandtime.ui:53
#, kde-format
msgid "&Time server:"
msgstr ""

#. i18n: ectx: property (whatsThis), widget (KDatePicker, cal)
#: dateandtime.ui:86
#, kde-format
msgid "Here you can change the system date's day of the month, month and year."
msgstr "এখানে আপনি সিস্টেম সময়ের দিন, মাস এবং বছর পরিবর্তন করতে পারেন।"

#. i18n: ectx: attribute (title), widget (QWidget, tab_2)
#: dateandtime.ui:122
#, fuzzy, kde-format
msgid "Time Zone"
msgstr "টাইম জোন সমস্যা"

#. i18n: ectx: property (text), widget (QLabel, label)
#: dateandtime.ui:128
#, fuzzy, kde-format
msgid "To change the local time zone, select your area from the list below."
msgstr "টাইম জোন পরিবর্তন করতে নীচের তালিকা থেকে আপনার অঞ্চলটি নির্বাচন করুন:"

#. i18n: ectx: property (text), widget (QLabel, m_local)
#: dateandtime.ui:151
#, fuzzy, kde-format
msgid "Current local time zone:"
msgstr "বর্তমান টাইম জোন:"

#. i18n: ectx: property (placeholderText), widget (KTreeWidgetSearchLine, tzonesearch)
#: dateandtime.ui:161
#, kde-format
msgid "Search…"
msgstr ""

#: dtime.cpp:61
#, kde-format
msgid ""
"No NTP utility has been found. Install 'ntpdate' or 'rdate' command to "
"enable automatic updating of date and time."
msgstr ""

#: dtime.cpp:91
#, fuzzy, kde-format
msgid ""
"Here you can change the system time. Click into the hours, minutes or "
"seconds field to change the relevant value, either using the up and down "
"buttons to the right or by entering a new value."
msgstr ""
"এখানে আপনি সিস্টেম সময় পরিবর্তন করতে পারেন। ক্লিক ভেতরে টি অথবা সেকেন্ড ক্ষেত্রে "
"প্রতি পরিবর্তন টি প্রাসঙ্গিক মান হয় ব্যবহার করে টি উপর এবং নীচে প্রতি টি ডানদিক "
"অথবা দ্বারা প্রবেশ করা একটি নতুন মান."

#: dtime.cpp:113
#, fuzzy, kde-format
msgctxt "%1 is name of time zone"
msgid "Current local time zone: %1"
msgstr "বর্তমান টাইম জোন:"

#: dtime.cpp:116
#, fuzzy, kde-format
msgctxt "%1 is name of time zone, %2 is its abbreviation"
msgid "Current local time zone: %1 (%2)"
msgstr "বর্তমান টাইম জোন:"

#: dtime.cpp:203
#, kde-format
msgid ""
"Public Time Server (pool.ntp.org),        asia.pool.ntp.org,        europe."
"pool.ntp.org,        north-america.pool.ntp.org,        oceania.pool.ntp.org"
msgstr ""

#: dtime.cpp:274
#, kde-format
msgid "Unable to contact time server: %1."
msgstr ""

#: dtime.cpp:278
#, kde-format
msgid "Can not set date."
msgstr "তারিখ নির্ধারণ করা যায়নি।"

#: dtime.cpp:281
#, fuzzy, kde-format
msgid "Error setting new time zone."
msgstr "নতুন টাইম জোন নির্ধারণ করাকালীন সমস্যা!"

#: dtime.cpp:281
#, fuzzy, kde-format
msgid "Time zone Error"
msgstr "টাইম জোন সমস্যা"

#: dtime.cpp:299
#, fuzzy, kde-format
msgid ""
"<h1>Date & Time</h1> This system settings module can be used to set the "
"system date and time. As these settings do not only affect you as a user, "
"but rather the whole system, you can only change these settings when you "
"start the System Settings as root. If you do not have the root password, but "
"feel the system time should be corrected, please contact your system "
"administrator."
msgstr ""
"<h1>তারিখ এবং সময়</h1> এই নিয়ন্ত্রণ মডিউল পারে ব্যবহৃত প্রতি সেট,সমষ্টি,মান "
"নির্ধারণ টি সিস্টেম তারিখ এবং সময়   এই বৈশিষ্ট্যাবলী নয় শুধু আপনি   একটি "
"ব্যবহারকারী কিন্তু বরং টি গোটা সিস্টেম আপনি পারে শুধু পরিবর্তন এই বৈশিষ্ট্যাবলী যখন "
"আপনি শুরু টি নিয়ন্ত্রণ মাঝখান   মূল যদি আপনি টি মূল পাসওয়ার্ড কিন্তু টি সিস্টেম সময় "
"উচিত সংশোধিত অনুগ্রহ করে পরিচিতি আপনার সিস্টেম অ্যাডমিনস্ট্রেটর."

#: main.cpp:49
#, kde-format
msgid "KDE Clock Control Module"
msgstr "কে.ডি.ই. ঘড়ি নিয়ন্ত্রণ মডিউল"

#: main.cpp:53
#, kde-format
msgid "(c) 1996 - 2001 Luca Montecchiani"
msgstr ""

#: main.cpp:55
#, kde-format
msgid "Luca Montecchiani"
msgstr ""

#: main.cpp:55
#, kde-format
msgid "Original author"
msgstr "মূল লেখক"

#: main.cpp:56
#, kde-format
msgid "Paul Campbell"
msgstr ""

#: main.cpp:56
#, kde-format
msgid "Current Maintainer"
msgstr "বর্তমান রক্ষণাবেক্ষণকারী"

#: main.cpp:57
#, kde-format
msgid "Benjamin Meyer"
msgstr ""

#: main.cpp:57
#, kde-format
msgid "Added NTP support"
msgstr ""

#: main.cpp:60
#, fuzzy, kde-format
msgid ""
"<h1>Date & Time</h1> This control module can be used to set the system date "
"and time. As these settings do not only affect you as a user, but rather the "
"whole system, you can only change these settings when you start the System "
"Settings as root. If you do not have the root password, but feel the system "
"time should be corrected, please contact your system administrator."
msgstr ""
"<h1>তারিখ এবং সময়</h1> এই নিয়ন্ত্রণ মডিউল পারে ব্যবহৃত প্রতি সেট,সমষ্টি,মান "
"নির্ধারণ টি সিস্টেম তারিখ এবং সময়   এই বৈশিষ্ট্যাবলী নয় শুধু আপনি   একটি "
"ব্যবহারকারী কিন্তু বরং টি গোটা সিস্টেম আপনি পারে শুধু পরিবর্তন এই বৈশিষ্ট্যাবলী যখন "
"আপনি শুরু টি নিয়ন্ত্রণ মাঝখান   মূল যদি আপনি টি মূল পাসওয়ার্ড কিন্তু টি সিস্টেম সময় "
"উচিত সংশোধিত অনুগ্রহ করে পরিচিতি আপনার সিস্টেম অ্যাডমিনস্ট্রেটর."

#: main.cpp:113
#, kde-format
msgid "Unable to authenticate/execute the action: %1, %2"
msgstr ""

#: main.cpp:133
#, kde-format
msgid "Unable to change NTP settings"
msgstr ""

#: main.cpp:144
#, kde-format
msgid "Unable to set current time"
msgstr ""

#: main.cpp:154
#, kde-format
msgid "Unable to set timezone"
msgstr ""

#~ msgid "kcmclock"
#~ msgstr "kcmclock"

#, fuzzy
#~ msgid "Date/Time Error"
#~ msgstr "টাইম জোন সমস্যা"
