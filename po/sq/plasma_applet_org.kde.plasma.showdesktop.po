# Albanian translation for kdeplasma-addons
# Copyright (c) 2009 Rosetta Contributors and Canonical Ltd 2009
# This file is distributed under the same license as the kdeplasma-addons package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2009.
#
msgid ""
msgstr ""
"Project-Id-Version: kdeplasma-addons\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-12-26 00:54+0000\n"
"PO-Revision-Date: 2009-03-13 13:10+0000\n"
"Last-Translator: Vilson Gjeci <vilsongjeci@gmail.com>\n"
"Language-Team: Albanian <sq@li.org>\n"
"Language: sq\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Launchpad-Export-Date: 2011-04-22 01:38+0000\n"
"X-Generator: Launchpad (build 12883)\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#: package/contents/ui/MinimizeAllController.qml:17
#, kde-format
msgctxt "@action:button"
msgid "Restore All Minimized Windows"
msgstr ""

#: package/contents/ui/MinimizeAllController.qml:18
#, kde-format
msgctxt "@action:button"
msgid "Minimize All Windows"
msgstr ""

#: package/contents/ui/MinimizeAllController.qml:20
#, kde-format
msgctxt "@info:tooltip"
msgid "Restores the previously minimized windows"
msgstr ""

#: package/contents/ui/MinimizeAllController.qml:21
#, kde-format
msgctxt "@info:tooltip"
msgid "Shows the Desktop by minimizing all windows"
msgstr ""

#: package/contents/ui/PeekController.qml:14
#, kde-format
msgctxt "@action:button"
msgid "Peek at Desktop"
msgstr ""

#: package/contents/ui/PeekController.qml:15
#, kde-format
msgctxt "@action:button"
msgid "Stop Peeking at Desktop"
msgstr ""

#: package/contents/ui/PeekController.qml:17
#, kde-format
msgctxt "@info:tooltip"
msgid "Moves windows back to their original positions"
msgstr ""

#: package/contents/ui/PeekController.qml:18
#, kde-format
msgctxt "@info:tooltip"
msgid "Temporarily shows the desktop by moving windows away"
msgstr ""

#, fuzzy
#~| msgid "Show the Desktop"
#~ msgid "Show Desktop"
#~ msgstr "Shfaq Desktopin"

#, fuzzy
#~| msgid "Show the Desktop"
#~ msgid "Show the Plasma desktop"
#~ msgstr "Shfaq Desktopin"

#~ msgid "Minimize all open windows and show the Desktop"
#~ msgstr "Minimizon të gjitha dritaret e hapura dhe shfaq Desktopin"
