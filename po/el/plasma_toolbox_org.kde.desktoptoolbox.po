# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Dimitris Kardarakos <dimkard@gmail.com>, 2014, 2015.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-03-08 02:24+0000\n"
"PO-Revision-Date: 2015-11-18 16:24+0200\n"
"Last-Translator: Dimitris Kardarakos <dimkard@gmail.com>\n"
"Language-Team: Greek <kde-i18n-el@kde.org>\n"
"Language: el\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 2.0\n"

#: contents/ui/ToolBoxContent.qml:265
#, kde-format
msgid "Choose Global Theme…"
msgstr ""

#: contents/ui/ToolBoxContent.qml:272
#, kde-format
msgid "Configure Display Settings…"
msgstr ""

#: contents/ui/ToolBoxContent.qml:293
#, kde-format
msgctxt "@action:button"
msgid "More"
msgstr ""

#: contents/ui/ToolBoxContent.qml:308
#, kde-format
msgid "Exit Edit Mode"
msgstr ""

#~ msgid "Lock Screen"
#~ msgstr "Κλείδωμα οθόνης"

#~ msgid "Leave"
#~ msgstr "Έξοδος"
